---
title: "Python数据结构-collections"
date: 2017-11-20T13:55:31+08:00
draft: true
tags: ["数据结构"]
---

词典和列表可以满足一切的需求, 但collection可以更好的满足.
<!--more-->



就像[python doc](https://docs.python.org/3/library/collections.html)的第一句话说的  

This module implements specialized container datatypes providing alternatives to Python’s general purpose built-in containers, dict, list, set, and tuple.  

这个模块是提供一个与python内置模块二选一的机会.主要有一下数据类型:  


| 模块 | 解释 |
| :------ |:--- |
| namedtuple()  | 这是个工厂方法,生成有命名字段的tuple子类 |
| deque(双向队列) | 两端快速入队, 出对的队列|
| ChainMap | 为多个映射创建单一视图的类字典类型 |
| Counter | dict子类, 可以用来计算hash对象的个数 |
| OrderedDict  | dict子类, 顺序的添加词典 |
| defaultdict | 调用一个工厂函数来为dict的values缺失提供值 |
| UserDict | 将字典包裹起来使得创建字典的子类更容易 |
| UserList | 将列表对象包裹起来使得创建列表的子类更容易 |
| UserString | 将字符串对象包裹起来使得创建字符串的子类更容易 |  


## namedtuple
可以用来快速生成对象, 流畅的python刚开始的例子, 用来快速生成有花色和大小的Card类,
{{< highlight javascript>}}
import collections
Card = collections.namedtuple('Card', ['rank', 'suit'])

class FrenchDeck:
    ranks = [str(n) for n in range(2, 11)] + list('JQKA')
    suits = 'spades diamonds clubs hearts'.split()

    def __init__(self):
        self._cards = [Card(rank, suit) for suit in self.suits for rank in self.ranks]

     def __len__(self):
         return len(self._cards)

     def __getitem__(self, position):
         return self._cards[position]
{{</ highlight >}}


## deque双端队列
实现一个跑马灯
{{< highlight javascript>}}
import sys
import time
from collections import deque

fancy_loading = deque('>--------------------')

while True:
    print('\r%s' % ''.join(fancy_loading)),
    fancy_loading.rotate(1)
    sys.stdout.flush()
    time.sleep(0.08)


# result    
>--------------------
->-------------------
-->------------------
--->-----------------
---->----------------
----->---------------
------>--------------
------->-------------
-------->------------
--------->-----------
---------->----------
----------->---------
------------>--------

{{</ highlight >}}




## UserDict, UserList, UserString
主要用来继承python内置的数据结构, 由于dict, list, str 通过c写的, 父类不会调用重写的子类方法, 例如, 这里dict不会调用子类自己实现的__getitem__

{{< highlight javascript>}}
from collections import UserDict
class Point(dict):
    def __getitem__(self, item):
        return 42

class _Point(UserDict):
    def __getitem__(self, item):
        return 42

"""dict是C写的, 没有办法调用python重写的, build-in一般都是C写的"""
p = Point(y=22)
p['x'] = 12
print(p)
print(p['x'])
p2 = {}
p2.update(p)
print(p2)
print('-'*100)
p = _Point(y=22)
p['x'] = 12
print(p)
print(p['x'])
p2 = {}
p2.update(p)
print(p2)

{'y': 22, 'x': 12}
42
{'y': 22, 'x': 12}
----------------------------------------------------------------------------------------------------
{'y': 22, 'x': 12}
42
{'y': 42, 'x': 42}
{{</ highlight >}}



## ChainMap
map Chain, 词典组成的列表

{{< highlight javascript>}}
from collections import ChainMap

a = {'name': 'wangfeng', 'age': 17}
b = {'like': 'dota'}
c = {'future': 'maybe', 'word': 'hard'}

chain = ChainMap(a, b, c)

"""result"""
ChainMap({'age': 17, 'name': 'wangfeng'}, {'like': 'dota'}, {'future': 'maybe', 'word': 'hard'})
{{</ highlight >}}



## Counter
dict子类, 元素是key, 元素出现的次数是值
{{< highlight javascript>}}
from collections import Counter
c = Counter('this is a dog')

"""result"""
Counter({' ': 3, 's': 2, 'i': 2, 'o': 1, 'd': 1, 'g': 1, 'h': 1, 't': 1, 'a': 1})
{{</ highlight >}}


## OrderedDict
有序词典
{{< highlight javascript "linenos=inline">}}
d = {'banana': 3, 'apple': 4, 'pear': 1, 'orange': 2}
OrderedDict(sorted(d.items(), key=lambda t: t[0]))
"""result"""
OrderedDict([('apple', 4), ('banana', 3), ('orange', 2), ('pear', 1)])
{{</ highlight >}}

## defaultdict
defaultdict 和 setdefault 和 dict.get()区别  
setdefault 只有在key不存在的时候设置相应的value
defaultdict  直接给一个词典设置默认的value

比如我们想要统计一个列表中,每个单词出现的次数, 我们需要通过判断语句先判断是否这个单词出现在这个词典中
{{< highlight javascript>}}

l = ['apple', 'orange', 'banada', 'apple', 'orange', 'apple']
rst = {}
for each in l:
    if each not in rst:
        rst[each] = 1
    else:
        rst[each] = rst[each] + 1

"""result"""
{'banada': 1, 'orange': 2, 'apple': 3}
{{</ highlight >}}

引入了一个判断语句, 这样代码一定会被拉长, 但是假如可以在获取词典对应的元素的值的同时, 给这个值一个默认的数值, 就可以省去判断的代码.这时, setdefault就派上用场了
{{< highlight javascript>}}

l = ['apple', 'orange', 'banada', 'apple', 'orange', 'apple']
rst = {}
for each in l:
    rst[each] = rst.setdefault(each, 0) + 1
print(rst)

"""result"""
{'banada': 1, 'orange': 2, 'apple': 3}
{{</ highlight >}}

既然可以在获得这个元素的时候默认定义一个值, 在声明的时候直接默认所有词典的值, 应该能提高更多的效率, defaultdict可以实现

{{< highlight javascript>}}
from collections import defaultdict
l = ['apple', 'orange', 'banada', 'apple', 'orange', 'apple']
"""这里定义的是词典value的类型"""
rst = defaultdict(int)
for each in l:
    rst[each] = rst[each] + 1
print(rst)
"""result"""
{'banada': 1, 'orange': 2, 'apple': 3}
{{</ highlight >}}

注意, 这里rst[each]不可以写成rst.get(), 因为dict[]方法和defaultdict一样,如果找不到key对应的值, 会自动调用\__miss\__. 这里相当于重写了\__miss\__, 返回应类型的默认值.[python dict 文档](https://docs.python.org/3/library/collections.html#collections.OrderedDict)
