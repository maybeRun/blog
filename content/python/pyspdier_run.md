---
title: "pyspider[一]启动"
date: 2017-11-02T20:39:14+08:00
tags: ["pyspider"]
---


在我不会看源码的时候, 我就开始run了, 只不过, run的在久, 也有可能只是回到原点.
<!--more-->



run.py是整个函数的入口. 主要用来负责给接受参数并每个模块分配一个进程启动.


## cli模块
通过click收集数据, 存入kwargs
这里传入的参数可以参考[pyspider doc](http://docs.pyspider.org/en/latest/Command-Line/).
之后,收集参数传入各个模块
创建taskdb, resultdb, projectdb, 这里支持多个数据库, mysql, mongodb
{{< highlight javascript "linenos=inline">}}
for db in ('taskdb', 'projectdb', 'resultdb'):
    if kwargs[db] is not None:
        continue
    if os.environ.get('MYSQL_NAME'):
        kwargs[db] = utils.Get(lambda db=db: connect_database(
            'sqlalchemy+mysql+%s://%s:%s/%s' % (
                db, os.environ['MYSQL_PORT_3306_TCP_ADDR'],
                os.environ['MYSQL_PORT_3306_TCP_PORT'], db)))
    elif os.environ.get('MONGODB_NAME'):
        kwargs[db] = utils.Get(lambda db=db: connect_database(
            'mongodb+%s://%s:%s/%s' % (
                db, os.environ['MONGODB_PORT_27017_TCP_ADDR'],
                os.environ['MONGODB_PORT_27017_TCP_PORT'], db)))
{{</ highlight >}}

甚至,当什么都不设置的时候, 调用sqllite  

{{< highlight javascript "linenos=inline">}}
kwargs[db] = utils.Get(lambda db=db: connect_database('sqlite+%s:///%s/%s.db' % (
             db, kwargs['data_path'], db[:-2])))
{{</ highlight >}}

cli模块不光设置数据库, 消息队列也在这里初始化.  

{{< highlight javascript "linenos=inline">}}
if kwargs.get('message_queue'):
    pass
elif kwargs.get('amqp_url'):
    kwargs['message_queue'] = kwargs['amqp_url']
elif os.environ.get('RABBITMQ_NAME'):
    kwargs['message_queue'] = ("amqp://guest:guest@%(RABBITMQ_PORT_5672_TCP_ADDR)s"
                               ":%(RABBITMQ_PORT_5672_TCP_PORT)s/%%2F" % os.environ)

{{</ highlight >}}

接下来创建5个队列, 这5个队列很重要, 与pyspider的各个模块有关   
- newtask_queue  
- status_queue  
- scheduler2fetcher  
- fetcher2processor  
- processor2result  

在pyspdier的各个模块会详细写这几个队列, 这里可以大概写一个之间的关系.  
1. scheduler读取status_queue, 检查状态, 新的任务放入newtask_queue  
2. scheduler读取newtask_queue放入scheduler2fetcher  
3. fetcher读取scheduler2fetcher, 放入fetcher2processor  
4. process读取fetcher2processor, 放入processor2result或status_queue  
5. result读取processor2result  

{{< highlight javascript "linenos=inline">}}
for name in ('newtask_queue', 'status_queue', 'scheduler2fetcher',
             'fetcher2processor', 'processor2result'):
    if kwargs.get('message_queue'):
        kwargs[name] = utils.Get(lambda name=name: connect_message_queue(
            name, kwargs.get('message_queue'), kwargs['queue_maxsize']))
    else:
        kwargs[name] = connect_message_queue(name, kwargs.get('message_queue'),
                                             kwargs['queue_maxsize'])
{{</ highlight >}}

## result, webui, scheduler, process模块

各个模块具体功能在模块内部写, 这里主要看实例化对象传入的参数.

#### result  

| 参数 | 说明 |
| :------ |:--- |
| resultdb | 存储爬去的数据的数据库 |
| inqueue | 获得爬去的数据的队列(processor2result) |

#### process

| 参数 | 说明 |
| :------ |:--- |
| projectdb | process数据库 |
| inqueue | process获得数据队列(fetcher2processor) |
| result_queue | result模块处理的数据插入的队列 |
| newtask_queue | scheduler模块处理的数据插入队列 |
| status_queue | scheduler模块处理的数据插入队列 |
| enable_stdout_capture | 和输出日志有关的东西, 不管 |
| process_time_limit | process处理时间限制 |

#### webui

| 参数 | 说明 |
| :------ |:--- |
| taskdb, projectdb, resultdb | 数据库连接 |
| newtask_queue, status_queue, scheduler2fetcher, fetcher2processor, processor2result | 队列连接 |
| fetcher_rpc, scheduler_rpc | rpc连接, 用来和爬虫交互等 |

#### scheduler

| 参数 | 说明 |
| :------ |:--- |
| taskdb, projectdb, resultdb | 数据库连接 |
| newtask_queue, status_queue, out_queue | 队列连接(newtask_queue, status_queue, scheduler2fetcher) |
| INQUEUE_LIMIT | 插入队列的限制,应该和令牌桶算法有关 |
| DELETE_TIME | 删除的时间限制 |
| ACTIVE_TASKS | 活跃task |
| LOOP_LIMIT | 一次执行task数量 |
| FAIL_PAUSE_NUM | 超过多少个pause |
