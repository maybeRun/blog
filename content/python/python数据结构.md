---
title: "Python数据结构"
date: 2017-11-20T14:49:50+08:00
draft: true
tags: ["数据结构"]
---
python 标准库里有的, 是dota, 吃鸡, 王者荣耀学不到的.
<!--more-->

python标准库的目录.
Data Types  
8.1. datetime — Basic date and time types  
8.2. calendar — General calendar-related functions  
8.3. collections — Container datatypes  
8.4. collections.abc — Abstract Base Classes for Containers  
8.5. heapq — Heap queue algorithm  
8.6. bisect — Array bisection algorithm  
8.7. array — Efficient arrays of numeric values  
8.8. weakref — Weak references  
8.9. types — Dynamic type creation and names for built-in types  
8.10. copy — Shallow and deep copy operations  
8.11. pprint — Data pretty printer  
8.12. reprlib — Alternate repr() implementation  
8.13. enum — Support for enumerations  

以前和同事说, 很多书看完目录就基本上可以看完了, 但是没人相信. 我特别喜欢看目录, 因为我觉得, 弄清楚作者书写的整体思路, 理清楚整体的逻辑是一件特别有意思的事情. 而对知识的切分也是特别重要的一件事情.  

data type主要出现在第八个章节, 前七个章节主要说内置的方法, 常量, 内置数据类型, 内置异常, 文本处理, 二进制转换.  

为什么要这么安排, 因为这样最容易从开始学习一门语言.  

## Data Types  
时间-日历

容器-容器基类-容器的一种基本的实现(堆)-容器的查找算法-数组-引用-创建名称-复制-输出-枚举
基本上这一个章节包括了data的产生, 到引用, 到命名, 到变形,到输出.

这样的目录结构特别容易被人使用  

在看golang, 按照字母排序, 心好累.
