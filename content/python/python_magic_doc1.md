---
title: "why python"
subtitle: 所有下划线函数汇总
date: 2017-11-20T20:25:29+08:00
draft: true
---
你如果认识从前的我，也许你会原谅现在的我      by:张爱玲
<!--more-->

## 流畅的python
看了好几次流畅的python, 开始对python到底是什么, 为什么有了一些理解, 在python中, 带有下划线的函数, 有着特殊的作用, 也构成了不一样的python语言特性.

## 操作符和运算符

| 关键字 | 解释 |
| :------ |:--- |
| \__cmp\__(self, other) | 在比较的时候调用 |
| \__eq\__(self, other) | 定义了等号的行为 ==  |
| \__ne\__(self, other) | 定义了不等号的行为 != |
| \__lt\__(self, other) | 定义了小于号的行为 <  |
| \__gt\__(self, other) | 定义了大于等于号的行为 >= |
| \__add\__(self, other) | 实现加法 |
| \__sub\__(self, other) | 实现减法 |
| \__mul\__(self, other) |  实现乘法 |
| \__floordiv\__(self, other) | 实现 // 符号实现的整数除法 |
| \__div\__(self, other) | 实现 / 符号实现的除法 |
| \__truediv\__(self, other) | 实现真除法 |
| \__mod\__(self, other) | 实现取模算法% |
| \__divmod\___(self, other) | 实现内置 divmod() 算法 |
| \__pow\__(self, other) | 实现使用 ** 的指数运算 |
| \__lshift\__(self, other) | 实现使用 << 的按位左移动 |
| \__rshift\__(self, other) |  实现使用 >> 的按位左移动 |
| \__and\__(self, other) |  实现使用 & 的按位与 |
| \__or\__(self, other) | 实现使用 | 的按位或 |
| \__xor\__(self, other) |  实现使用 ^ 的按位异或 |

下面是一个比较字符串大小的例子, \__new\__方法会在下面说明.
{{< highlight javascript >}}
class Word(str):
'''存储单词的类，定义比较单词的几种方法'''

    def __new__(cls, word):
        """
        注意我们必须要用到__new__方法，因为str是不可变类型
       所以我们必须在创建的时候将它初始化
        """
        if ' ' in word:
            print "Value contains spaces. Truncating to first space."
            word = word[:word.index(' ')] #单词是第一个空格之前的所有字符
        return str.__new__(cls, word)

    def __gt__(self, other):
        return len(self) > len(other)
    def __lt__(self, other):
        return len(self) < len(other)
    def __ge__(self, other):
        return len(self) >= len(other)
    def __le__(self, other):
        return len(self) <= len(other)
{{</ highlight >}}




## 类的表示
| 关键字 | 解释 |
| :------ |:--- |
| \__new\__(cls, [kwargs]) | 返回一个类实例, 继承一些不可变的类(比如int, str等)的时候使用 |
| \__hash\__(cls, [kwargs]) | 定义当 hash() 调用的时候的返回值 |
|\__repr\__| 机器可读的输出 |
|\__str\__| 人类可读的输出, 打印对象的时候展示|
|\__slots\__| 定义类的属性, 不允许动态的扩充 |

### \__new\__
类在实例化的时候先调用自己的\__new\__, 如果没有就调用父类的\__new\__, 也就是说, Num子类被父类\__new\__创建, Num的属性i被子类\__init\__赋值.
{{< highlight javascript >}}
class Num(int):

    def __init__(self, i):
        self.i = abs(i)

n = Num(-3)
print(n)
print(n.i)
"""result"""
-3
3
{{</ highlight >}}

{{< highlight javascript >}}
class Student:
    __slots__ = ['name']
    def __init__(self, name):
        self.name = name


s = Student('wangfeng')
s.age = 17

'''result'''
Traceback (most recent call last):
  File "ts.py", line 8, in <module>
    s.age = 17
AttributeError: 'Student' object has no attribute 'age'
{{</ highlight >}}

## 访问控制
之前一直都不知道, 调用属性这件事, 可以细致到这样

| 关键字 | 解释 |
| :------ |:--- |
| \__getattribute\__ | 无条件地调用对类的实例实现属性访问 |
| \__get\__ | (描述器)descriptor |
|\__set\__| (描述器)descriptor |
|\__getattr\__| 当属性查找在通常的地方没有找到该属性时调用|
|\__dict\__| 类属性 |
|\__class\__| 返回实例属于那个类 |

关于类属性调用, 我们一般的思维都是 实例 - 类 - 基类.但其实, 属性调用的顺序是基类 - 类 - 实例.(这个真的没有找到文档, 以后再说吧)
可是, 很多时候, 我们希望直接调用的是实例的属性, 而不是通过一层层查找下来的属性.[python描述器指南](http://python.usyiyi.cn/documents/python_352/howto/descriptor.html)中,
```
如果实例的字典具有与非数据描述器(只实现了__get__的类)相同的名称的键，则字典的键优先。
```
也就是说, 如果只实现了\__get\__那么就会优先调取实例的属性 .   

可是还有一些情况, 我们想通过改变实例的属性, 改变类的属性, 这个时候的调用顺序不同, 先找基类和父类, 如果没有在修改实例.下面是通过修改实例属性修改类属性.
{{< highlight javascript >}}
class RevealAccess(object):
    """A data descriptor that sets and returns values
       normally and prints a message logging their access.
    """

    def __init__(self):
        self.val = 0


    def __get__(self, obj, objtype):
        print('Retrieving', self.val)
        return self.val

    def __set__(self, obj, val):
        print('Updating', self.val)
        self.val = val

r = RevealAccess()
print(r.val)
r.val = 100
r2 = RevealAccess()
print(r.val)
"""result"""
0
100
{{</ highlight >}}  

__总结__:如果不能写代码说明调用关系, 就很难说清楚这个问题. 但是逻辑上来讲, 只能通过改变获得属性的顺序来处理给不同对象(python 一切结对象)赋值的问题.  

另外, 改变调用顺序仅仅需要在\__getattribute\__实现就可以了.实现起来也很优雅.因为\__getattribute\__会调用描述器方法, 如果抛出异常在调用\__getattr\__.


## 自定义序列




| 关键字 | 解释 |
| :------ |:--- |   
| \__getitem\__ | self[key] |   
| \__setitem\__ |  |  
|\__delitem\__|  |  
|\__len\__| length()|  
|\__contains\__| in  |    



## 会话管理

\__enter\__, \__exit__\




{{< highlight javascript >}}
class Closer:
'''通过with语句和一个close方法来关闭一个对象的会话管理器'''

def __init__(self, obj):
    self.obj = obj

def __enter__(self):
    return self.obj # bound to target

def __exit__(self, exception_type, exception_val, trace):
    try:
        self.obj.close()
    except AttributeError: # obj isn't closable
        print 'Not closable.'
        return True # exception handled successfully
{{</ highlight >}}
