---
title: "Django连接mysql, 安装mysqlclient"
date: 2017-12-08T14:43:33+08:00
draft: true
tags: ["django"]
---
Django连接数据库可能报no module mysqldb.
不建议修改为pymysql, 因为这个包有一个bug, 版本不对, 还需要修改django源码, 部署不方便



建议安装mysqlclient
sudo apt-get install mysql-serve
sudo apt-get install libmysqlclient-dev
pip3 install mysqlclient
