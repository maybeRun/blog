---
title: "pyspider[二]scheduler"
date: 2017-11-11T15:42:06+08:00
draft: true
tags: ["pyspider"]
---
假如我有调度器, 我会回到每个和你在一起的地方, 拨慢每只表.
<!--more-->

## 从令牌桶开始
令牌桶算法是为了处理流量传输不均匀产生的, 思路大概就是, 既然流量的产生不均匀, 那我自己创造一个均匀的东西, 让流量随着这个东西的节奏发送.就像在一个桶里面通过一个rate产生令牌, 流量通过拿到令牌流通.具体的可以看[令牌桶算法-百度百科](https://baike.baidu.com/item/%E4%BB%A4%E7%89%8C%E6%A1%B6%E7%AE%97%E6%B3%95)  

pyspider令牌桶是scheduler用来控制流量. 但是, 这部分代码怎么都觉的没有真正实现到令牌个数和消息个数对比的层面,而仅仅是在调度过程中,当桶内令牌的数量大于传输的数据量时就直接通过, 等待桶内令牌数也就是rate * (now - last_update)大于数据量.找了一些其他的代码, 推荐下[令牌桶](https://github.com/kwsy/Flask-TrafficShape).

对于pyspider, 目前仅仅需要看懂注释就可以了
{{< highlight javascript >}}
class Bucket(object):
    '''
    rate: 每秒产生令牌数量
    burst: 一次性传输的数据量
    bucket: 令牌数量
    traffic flow control with token bucket
    '''

    update_interval = 30

    def __init__(self, rate=1, burst=None):
        self.rate = float(rate)
        if burst is None:
            self.burst = float(rate) * 10
        else:
            self.burst = float(burst)
        self.mutex = _threading.Lock()
        self.bucket = self.burst
        self.last_update = time.time()


    def get(self):
        '''Get the number of tokens in bucket'''
        now = time.time()
        if self.bucket >= self.burst:
            self.last_update = now
            return self.bucket
        """令牌数量"""
        bucket = self.rate * (now - self.last_update)
        self.mutex.acquire()
        if bucket > 1:
            self.bucket += bucket
            if self.bucket > self.burst:
                self.bucket = self.burst
            self.last_update = now
        self.mutex.release()
        return self.bucket

    def set(self, value):
        '''Set number of tokens in bucket'''
        self.bucket = value

    def desc(self, value=1):
        '''Use value tokens'''
        self.bucket -= value
{{</ highlight >}}





## task_queue
有三个对象__InQueueTask__,__PriorityTaskQueue__, __TaskQueue__.分别来说

#### InQueueTask
task对象.三个属性, 并且可以通过priority和exetime比较, 具体怎么比较不知道  
1. taskid  
2. priority  
3. exetime:the executed time of task in unix timestamp(task的执行时间)　　  
#### PriorityTaskQueue  
优先级队列.相同的taskid会被合并.  
1. queue: 放task的队列, 后面变成heapq(堆栈)  
2. queue_dict  用来去重  

方法  
1. _qsize()  
2. _put()  
3. _get()  
等

#### TaskQueue
任务队列
2-3都是上面PriorityTaskQueue对象  
1. mutex:锁  
2. priority_queue: exetime < now的task放入这个队列  
3. time_queue:time_queue的作用是什么？  
4. processing：应该是进行结构化的队列  
5. bucket  

从time_queue拿出来放入priority_queue
{{< highlight javascript >}}
now = time.time()
self.mutex.acquire()
while self.time_queue.qsize() and self.time_queue.top and self.time_queue.top.exetime < now:
    task = self.time_queue.get_nowait()
    task.exetime = 0
    self.priority_queue.put(task)
self.mutex.release()
{{</ highlight >}}  


从processing队列拿出来放入priority_queue
{{< highlight javascript >}}
def _check_processing(self):
       now = time.time()
       self.mutex.acquire()
       while self.processing.qsize() and self.processing.top and self.processing.top.exetime < now:
           task = self.processing.get_nowait()
           if task.taskid is None:
               continue
           task.exetime = 0
           self.priority_queue.put(task)
           logger.info("processing: retry %s", task.taskid)
       self.mutex.release()
{{</ highlight >}}

通过流量桶算法从priority_queue获得数据
{{< highlight javascript >}}
def get(self):
    '''Get a task from queue when bucket available'''
    if self.bucket.get() < 1:
        return None
    now = time.time()
    self.mutex.acquire()
    try:
        task = self.priority_queue.get_nowait()
        self.bucket.desc()
    except Queue.Empty:
        self.mutex.release()
        return None
    task.exetime = now + self.processing_timeout
    self.processing.put(task)
    self.mutex.release()
    return task.taskid
{{</ highlight >}}

## Project
一个项目对象，　就是pyspider上的每一个爬虫．有启动,暂停,更新,完成４个状态　　
__判断project状态__  
1. active    爬虫是否启动  
2. task_loaded: false  
这个状态和active一起用来判断是否需要从数据库获得数据, _load_tasks的作用是从taskdb读取数据插入到task_queue(scheduler一部分)里面      
{{< highlight javascript >}}
if project.active:
           if not project.task_loaded:
               self._load_tasks(project)
               project.task_loaded = True
       else:
           if project.task_loaded:
               project.task_queue = TaskQueue()
               project.task_loaded = False
{{</ highlight >}}
3. _selected_tasks: False  
4. _send_finished_event_wait
{{< highlight javascript >}}
这两个用来判断是否project任务是否完成
if project_cnt:
             project._selected_tasks = True
             project._send_finished_event_wait = 0

         # check and send finished event to project
         if not project_cnt and len(task_queue) == 0 and project._selected_tasks:
             # wait for self.FAIL_PAUSE_NUM steps to make sure all tasks in queue have been processed
             if project._send_finished_event_wait < self.FAIL_PAUSE_NUM:
                 project._send_finished_event_wait += 1
             else:
                 project._selected_tasks = False
                 project._send_finished_event_wait = 0

                 self._postpone_request.append({
                     'project': project.name,
                     'taskid': 'on_finished',
                     'url': 'data:,on_finished',
                     'process': {
                         'callback': 'on_finished',
                     },
                     "schedule": {
                         "age": 0,
                         "priority": 9,
                         "force_update": True,
                     },
                 })
{{</ highlight >}}
5. _send_on_get_info  
6. waiting_get_info  
主要解决运行时更新信息的问题，脚本更改时,更新脚本, 并且之前运行的部分一起更新  
7. _paused
8. _paused_time  
9. _unpause_last_seen  
10. paused(True or False)  
爬虫pause状态   

__到这里，四个状态完全包括．__

__project信息__  
1．scheduler　　　  
2. project_info    
3. active_tasks(双向队列): 用来存放task
4. md5sum(hash脚本)

project_info是一个词典, 存project信息    

{{< highlight javascript >}}
{
  'rate': 1,
  'script': '#!/usr/bin/env python\n# -*- encoding: utf-8 -*-\n# Created on 2017-11-20 12:15:07\n# Project: tt\n\nfrom pyspider.libs.base_handler import *\n\n\nclass Handler(BaseHandler):\n    crawl_config = {\n    }\n\n    @every(minutes=24 * 60)\n    def on_start(self):\n        headers={\n        \'Accept\':\'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\',\n        \'Accept-Encoding\':\'gzip, deflate\',\n        \'Accept-Language\':\'zh-CN,zh;q=0.8\',\n        \'Cache-Control\':\'max-age=0\',\n        \'Connection\':\'keep-alive\',\n        \'Cookie\':\'safedog-flow-item=1E684B33B3D0451CA49A2466584AD46E\',\n        \'Host\':\'www.gdjyfy.cn\',\n        \'Upgrade-Insecure-Requests\':\'1\',\n        \'User-Agent\':\'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36\',\n        \'Proxy-Connection\': \'keep-alive\'    \n    }\n        self.crawl(\'http://www.gdjyfy.cn/Article/Article.aspx?CategoryId=17&WebShieldSessionVerify=BsPMZaYmZBjC3TT5O1Jr\', callback=self.index_page)\n\n    @config(age=10 * 24 * 60 * 60)\n    def index_page(self, response):\n        for each in response.doc(\'a[href^="http"]\').items():\n            self.crawl(each.attr.href, callback=self.detail_page)\n\n    @config(priority=2)\n    def detail_page(self, response):\n        return {\n            "url": response.url,\n            "title": response.doc(\'title\').text(),\n        }\n',
  'status': 'TODO',
  'comments': None,
  'name': 'aa',
  'burst': 3,
  'group': None,
  'updatetime': 1511154937.5678847}
{{</ highlight >}}

### scheduler
这里有一千多行代码．ThreadBaseScheduler应该是在Scheduler基础上加了多线程的功能,　大部分仍然调用Scheduler部分的代码, 之后再看


| 参数 | 备注 | 默认值　|
| :------ |:--- |:--- |
| UPDATE_PROJECT_INTERVAL | 更新project的时间间隔 |5 * 60 |
| default_schedule | 默认调度配置 |{'priority': 0,'retries': 3,'exetime': 0,'age': -1,'itag': None}|
| LOOP_LIMIT |　从队列拿出task的数量　| 1000 |
| LOOP_INTERVAL |　scheduler的run循环间隔　| 0.1 |
| ACTIVE_TASKS |　从队列拿出task的数量　| Project的task_queue大小 |
| INQUEUE_LIMIT |　应该是project的active_tasks的数量限制　| 0 |
| EXCEPTION_LIMIT | 出现except次数　| 3 |
| DELETE_TIME |　删除时间　| 24*60*60 |
| DEFAULT_RETRY_DELAY |　设置重试　|  |
| FAIL_PAUSE_NUM |　暂停数量，超过这个数量暂停　| 10 |
| PAUSE_TIME |　暂停时间　| 5*60 |
| UNPAUSE_CHECK_NUM |　重启测试数量　| 3 |
| TASK_PACK |　任务状态　| 1,2,3 |  

__scheduler属性__  
taskdb, projectdb, resultdb数据库连接和队列  
1. taskdb  
2. projectdb  
3. resultdb  
4. newtask_queue: newtask_queue, 新的task队列, process也会产生一些放入       
5. status_queue: process解析之后某个状态放入status_queue    
6. out_queue: scheduler2fetcher, 给fetcher组件的队列  

__存储__  
7．_send_buffer: 如果out_queue超过限制,会扔进_send_buffer
{{< highlight javascript >}}
try:
    self.out_queue.put_nowait(task)
except Queue.Full:
    if force:
        self._send_buffer.appendleft(task)
    else:
        raise
{{</ highlight >}}
8. projects: project实例字典  
9. _postpone_request: 这个东西是用来干嘛?

__状态__   
10. _force_update_project: 更新project      
11. _last_update_project: 更新project时间    　　  
12. _last_tick:　控制crontab 　

## scheduler流程

主要是run_once的代码, 总结来说,  

_update_projects:更新projcet,　从数据库拿task放入scheduler的project.task_queue  
_check_task_done: 检查status_queue(和process模块有关), 放入project.task_queue  
_check_request: 检查_postpone_request和newtask_queue, 放入project.task_queue  
_check_select: 从project.task_queue拿出来task放入fetcher队列
{{< highlight javascript >}}
def run_once(self):    
  '''comsume queues and feed tasks to fetcher, once'''   
  self._update_projects()   
  self._check_task_done()    
  self._check_request()    
  while self._check_cronjob():        
      pass    
  self._check_select()    
  self._check_delete()    
  self._try_dump_cnt()
{{</ highlight >}}
#### _update_projects()
更新project信息, 实例化project并且存入scheduler的self.projects.

{{< highlight javascript >}}
for project in self.projectdb.check_update(self._last_update_project):           
  self._update_project(project)
{{</ highlight >}}

#### _update_project()
发送一个taskid为_on_get_info给fetcher队列(用来更新project), 从数据库读取task,插入到self.project.task_queue
{{< highlight javascript >}}
if project._send_on_get_info:          
    # update project runtime info from processor by sending a _on_get_info          
    # request, result is in status_page.track.save          
    project._send_on_get_info = False          
    self.on_select_task({              
      'taskid': '_on_get_info',              
      'project': project.name,              
      'url': 'data:,_on_get_info',              
      'status': self.taskdb.SUCCESS,              
      'fetch': {                  
        'save': self.get_info_attributes,},              
        'process': {                  
          'callback': '_on_get_info',              },          })
          if project.active:     
          if not project.task_loaded:     
            # _load_tasks 就是从数据库取task          
            self._load_tasks(project)          
            project.task_loaded = True
{{</ highlight >}}

另外, webui更新project的交互的实现就是通过rpc触发修改_force_update_project的function.
{{< highlight javascript >}}
def update_project():    self._force_update_project = Trueapplication.register_function(update_project, 'update_project')
{{</ highlight >}}
#### _check_task_done()
检查status_queue. 叫_check_task_done的原因可能是因为这个队列的task是通过process模块产生, 检查是否正确.
{{< highlight javascript >}}
while True:          if task.get('taskid') == '_on_get_info' and 'project' in task and 'track' in task:    if task['project'] not in self.projects:        continue    project = self.projects[task['project']]    project.on_get_info(task['track'].get('save') or {})    logger.info(        '%s on_get_info %r', task['project'], task['track'].get('save', {})    )        continue    # 检测task是否满足需求    elif not self.task_verify(task):        continue    # 如果是新的task,    self.on_task_status(task)
{{</ highlight >}}

#### n_task_status
on_task_status调用on_task_done和on_task_failed, 并且把task插入active_tasks
{{< highlight javascript >}}
if procesok:    ret = self.on_task_done(task)else:    ret = self.on_task_failed(task) self.projects[task['project']].active_tasks.appendleft((time.time(), task))
{{</ highlight >}}
on_task_done把task放入self.project.status_queue, 并且更新数据库
on_task_failed判断是next_exetime, 如果小于0插入数据库task的status为fail, 否则插入插入数据
库放入self.project.task_queue

#### _check_request

从_postpone_request 和 newtask_queue 拿到task执行 on_request, _postpone_request这个队列用来存储正在processing状态的task,
可能是说, 在执行但是产生修改的task
{{< highlight javascript >}}
for task in self._postpone_request:  if self.projects[task['project']].task_queue.is_processing(task['taskid']):         todo.append(task)     else:         # 对于老的         self.on_request(task)self._postpone_request = todowhile len(tasks) < self.LOOP_LIMIT:           try:               task = self.newtask_queue.get_nowait()           except Queue.Empty:               breakfor task in itervalues(tasks):    self.on_request(task)
{{</ highlight >}}
on_request从数据库读取oldtask,如果存在执行on_old_request, 如果不存在执行on_new_request

#### on_old_request

判断老的task是否需要重新爬去或者取消, 更新数据库, 插入self.project.task_queue

#### on_new_request

插入task到数据库, 插入task到self.project.task_queue

#### _check_cronjob

插入一个taskid为_on_cronjob的task给fetcher的队列,插入task到self.project.active_tasks
{{< highlight javascript >}}
def _check_cronjob(self):    """Check projects cronjob tick, return True when a new tick is sended"""    now = time.time()    self._last_tick = int(self._last_tick)    if now - self._last_tick < 1:        return False    self._last_tick += 1    for project in itervalues(self.projects):        if not project.active:            continue        if project.waiting_get_info:            continue        if int(project.min_tick) == 0:            continue        if self._last_tick % int(project.min_tick) != 0:            continue        self.on_select_task({            'taskid': '_on_cronjob',            'project': project.name,            'url': 'data:,_on_cronjob',            'status': self.taskdb.SUCCESS,            'fetch': {                'save': {                    'tick': self._last_tick,                },            },            'process': {                'callback': '_on_cronjob',            },        })    return True
{{</ highlight >}}
_check_select

从self.project.task_queue拿出task, 插入fetcher队列
{{< highlight javascript >}}
for project, taskid in taskids:    self._load_put_task(project, taskid)    
{{</ highlight >}}
剩下两个用来删除project和监控队列数量
