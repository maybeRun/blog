---
title: "Two Sum"
date: 2017-12-05T14:32:40+08:00
draft: true
tags: ["leetcode"]
---

Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.
Example:
{{< highlight javascript >}}
Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
{{</ highlight >}}
<!--more-->


我的代码, 一个查询和一个遍历, 最大时间复杂度为
$$
O(N^{2})
$$
{{< highlight javascript >}}
class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        for num in nums:
            other_nums = target - num
            if other_nums in nums and other_nums != num:
                return sorted([nums.index(num), nums.index(target - num)])

        for num in nums:
            other_nums = target - num

            if other_nums in nums:
                rst = [i for i, num in enumerate(nums) if num==other_nums]
                return [rst[0], rst[-1]]
{{</ highlight >}}


优化的时间复杂度
$$
O(N)
$$
{{< highlight javascript >}}
class Solution(object):
    def twoSum(self, nums, target):
        if len(nums) <= 1:
            return False
        rst = {}
        for i, num in enumerate(nums):
            other_num = target - num
            if num not in rst:
                rst[other_num] = i
            else:
                return [rst[num], i]
{{</ highlight >}}
