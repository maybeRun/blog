---
title: "冒泡排序"
subtitle: 北京大学－－计算导论与C语言基础
date: 2017-11-09T17:52:15+08:00
tags: ["作业", "sort"]
draft: true
---

{{< highlight javascript >}}
冒泡排序: 每次把最大(小)值冒出来.
def bubble_sort(nums):
    for i in range(len(nums)):
        for index, num in enumerate(nums):
            if index < len(nums)-1:
                if num > nums[index + 1]:
                    nums[index], nums[index+1] = nums[index+1], nums[index]
    return nums
{{</ highlight >}}
<!--more-->
