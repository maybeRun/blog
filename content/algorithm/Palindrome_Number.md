
---
title: "Palindrome_Number"
date: 2017-12-07T21:25:04+08:00
draft: true
tags: ["leetcode"]
---
Determine whether an integer is a palindrome. Do this without extra space.
<!--more-->

注意　-1不是回文数
我的时间复杂度Ｎ, 超过10%

{{< highlight javascript >}}
class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        x = str(x)
        for i in range(len(x)):
            if x[i] != x[len(x)-i-1]:
                return False
        return True
{{</ highlight >}}

可以优化下，　只需要遍历到中间就可以了超过20%
{{< highlight javascript >}}
class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        x = str(x)
        for i in range(int(len(x)/2)):
            if x[i] != x[len(x)-i-1]:
                return False
        return True
{{</ highlight >}}


最快答案　反转列表进行比较,反转尽让是比较快的操作．．
{{< highlight javascript >}}
class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        return str(x) == str(x)[::-1]
{{</ highlight >}}
