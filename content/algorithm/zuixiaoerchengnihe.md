---
title: "最小二乘拟和"
date: 2017-12-19T15:19:19+08:00
draft: true
---

<!--more-->
回归分析是求最相似回归曲线, 实际值与预测值差的平方和  
### 公式
$$
S(x) = \sum [y-f(x)]^2
$$
