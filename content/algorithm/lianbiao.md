---
title: "链表基础"
date: 2017-12-20T11:41:37+08:00
draft: true
---


链表逆序  
求两个链表的交点  
链表的节点交换  
链表求环  
链表重新构造  
复杂的链表复制  
排序链表合并  
<!--more-->

## 160. Intersection of Two Linked Lists
Write a program to find the node at which the intersection of two singly linked lists begins.

{{< highlight javascript >}}

    def getIntersectionNode(self, headA, headB):
        if headA and headB:
            A, B= headA, headB

            while A != B:
                A =  A.next if A else headB
                B =  B.next if B else headA

            return A
{{</ highlight >}}  



## 141. Linked List Cycle

Given a linked list, determine if it has a cycle in it.

Follow up:
Can you solve it without using extra space?


{{< highlight javascript >}}

class Solution(object):
    def hasCycle(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        try:
            slow = head
            fast = head.next
            while slow is not fast:
                slow = slow.next
                fast = fast.next.next
            return True
        except:
          return False
{{</ highlight >}}    

## 142. Linked List Cycle II
多了一个返回环入口节点. 总结下用了快慢指针和从meet到环入口节点的距离等于从开始到环入口节点的距离相等.
Given a linked list, return the node where the cycle begins. If there is no cycle, return null.

Note: Do not modify the linked list.          
{{< highlight javascript >}}  


class Solution(object):
    def detectCycle(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        try:
            slow = head
            fast = head.next
            while slow is not fast:
                slow = slow.next
                fast = fast.next.next
        except:
            return None
                slow = slow.next
        while head is not slow:
            head = head.next
            slow = slow.next

        return head   
{{</ highlight >}}  


## 86. Partition List
难度中等.
Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

You should preserve the original relative order of the nodes in each of the two partitions.

For example,
Given 1->4->3->2->5->2 and x = 3,
return 1->2->2->4->3->5.


{{< highlight javascript >}}
class Solution(object):
  def partition(self, head, x):
      h1 = l1 = ListNode(0)
      h2 = l2 = ListNode(0)
      while head:
          if head.val < x:
              l1.next = head
              l1 = l1.next
          else:
              l2.next = head
              l2 = l2.next
          head = head.next
      l2.next = None
      l1.next = h2.next
      return h1.next
{{</ highlight >}}


## 138. Copy List with Random Pointer
A linked list is given such that each node contains an additional random pointer which could point to any node in the list or null.

Return a deep copy of the list.
{{< highlight javascript >}}
def copyRandomList(self, head):
    dic = dict()
    m = n = head
    while m:
        dic[m] = RandomListNode(m.label)
        m = m.next
    while n:
        dic[n].next = dic.get(n.next)
        dic[n].random = dic.get(n.random)
        n = n.next
    return dic.get(head)
{{</ highlight >}}
## 21. Merge Two Sorted Lists
Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists.

{{< highlight javascript >}}
class Solution(object):

        """
        先把老节点和新节点做一个映射, 结果就是
        新节点的next = dict[老节点]
        新节点的random = dict[老节点]
        :type head: RandomListNode
        :rtype: RandomListNode
        """

        def copyRandomList(self, head):
            dic = dict()
            m = n = head
            while m:
                dic[m] = RandomListNode(m.label)
                m = m.next
            while n:
                dic[n].next = dic.get(n.next)
                dic[n].random = dic.get(n.random)
                n = n.next
            return dic.get(head)


            class Solution(object):
                def mergeTwoLists(self, l1, l2):
                        if not l1 and not l2:
                            return None

                        node = guard_node = ListNode(0)

                        while l1 and l2:
                            if l1.val <= l2.val:
                                node.next = l1
                                l1 = l1.next
                            else:
                                node.next = l2
                                l2 = l2.next
                            node = node.next

                        if l1 or l2:
                            node.next = l1 or l2

                        return guard_node.next
{{</ highlight >}}
